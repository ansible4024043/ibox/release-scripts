#!/usr/bin/env bash

GIT_LOG_FILE="${1:?Please provide the git log file name as first argument}"
LAST_VERSION="${2:?Please provide the last version as second argument}"

echo "minor ${LAST_VERSION}"
exit 0
