#!/usr/bin/env bash
set -eu

GIT_REF_TAG="${1:?Please provide the reference tag as first argument}"

TMP_FILE=$(mktemp)

git log --no-merges --format="%s" "${GIT_REF_TAG}"..  | grep -v "semver:*" | grep -ve "^$" | sort | uniq > "${TMP_FILE}"

printf "# Changelog\n\n"

while read -r line; do
  echo "- ${line}"
done < "${TMP_FILE}"
